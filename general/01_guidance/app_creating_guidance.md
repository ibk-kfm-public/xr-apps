Introduction
=====
![application pipeline](https://gitlab.ethz.ch/ibk-kfm-public/xr-apps/-/raw/main/general/tools/images_videos/pipeline.jpg?ref_type=heads)
Follow the step-by-step instructions to create your own teaching website application. 

Material Preparation (by users)
=====
1. Geometrical Data

    Prepare and save all 2D and 3D geometries as follows:

    1. 3D Geoemtries: Each layer's 3D geometry should be exported as an independet joined OBJ file with a corresponding name, e.g., "mesh_concrete_wall.obj" or "mesh_reinforcement_beam.obj".

    2. 2D Geometries: Each layer's 2D geometry should be exported as an independent JSON file with a corresponding name, e.g.,  "arrow_force_external.json" or "lines_sut_wall_tie.json".

    [Grasshopper template to write JSON file](https://gitlab.ethz.ch/ibk-kfm-public/xr-apps/-/blob/main/general/tools/writeLinesToJson.gh?ref_type=heads)

2. Layer Information
        
    Preapare a CSV file with the following 5 columns:
    
    1. Layer Index: Numbering starts from 0 to n.
    2. Layer Name: This name should match the 2D and 3D geometry names without the extention, e.g., "mesh_concrete_wall" or "lines_sut_wall_tie".
    3. Type: The current version supports three types: "mesh", "lines" and "arrow".
    4. Material: Indicating materials for 3D geomtries or colors for 2D geometries, e.g., "steel", "concrete", "red".
    5. Display Name: This name will be displayed in the app for user interaction, e.g., "Druckstreben Hohlkasten" or "Bewehrung Strebe A".

    [layer csv example](https://gitlab.ethz.ch/ibk-kfm-public/xr-apps/-/raw/main/examples/2_torsion/OnlineResources/csv/layerInfo.csv?ref_type=heads)

3. Storytelling Configuration
    
    Prepare the storytelling steps configuration as follows:

    1. Columns: Each column, starting from column 0, contains binary data "0" and "1", where "0" means layer on and "1" means layer off.
    2. Rows: Each row corresponds to a layer from the layer CSV file from the previous step. The order of layers need to be consistent with the orders in layer CSV file. 
    3. Last Row: This row contains the display names of each storytelling step.

    [story csv example](https://gitlab.ethz.ch/ibk-kfm-public/xr-apps/-/raw/main/examples/2_torsion/OnlineResources/csv/storyInfo.csv?ref_type=heads)

4. Course General Introduction

    Prepare a text file containing the introduction of the course app. This will be displayed on the loading page. 


5. Detailed Story Step Descriptions with Math Equations

    For each step in the storytelling process, text and mathematical explainations will be displayed. This information should be provided in a markdown file. 

    [markdown example](https://gitlab.ethz.ch/ibk-kfm-public/xr-apps/-/raw/main/examples/2_torsion/OnlineResources/md/formula.md?ref_type=heads)

Uploading Online Resources (by users)
=====

Save all the prepared materials in their correspoinding folders and uploaded them to a publicly accessible online address.

![folder structure example](https://gitlab.ethz.ch/ibk-kfm-public/xr-apps/-/raw/main/general/tools/images_videos/folder_structure.png?ref_type=heads)

[online resource example](https://gitlab.ethz.ch/ibk-kfm-public/xr-apps/-/tree/main/examples/2_torsion/OnlineResources?ref_type=heads)

Customizing Application (By adding project in the json file on the webserver)
=====
1. Add new project name and resource folder to the json file: https://github.com/wenqian157/webServer/tree/main/StructureXRProjects. 
Following the format as: 
    {
        "name": "tBeam",
        "url":"https://raw.githubusercontent.com/wenqian157/webServer/main/tBeam"
    }

Customizing Application (By adding project in Unity)
=====
The next steps are:
1. Install Unity editor 2021.3.19, with Visual Studio 2019 or 2022, and WebGL build component.
2. Download the unity project from: https://gitlab.ethz.ch/ibk-kfm-public/xr-apps/-/tree/main/examples/0_unity?ref_type=heads
3. In Build Settings, change platform to WebGL
4. Download and import 2 plugins: https://assetstore.unity.com/packages/tools/gui/texdraw-51426; https://assetstore.unity.com/publishers/7690;
5. Open Scene "Open", find the component "RemoteCSVLoader", add project name and project url into the componennt "RemoteInfoLoader".
6. In the same "RemoteInfoLoader" script, input the project index of which you want to build.
7. Build both "Open" and "Main" scene as a WebGL project.
8. Deploy the WebGL application on the web host
